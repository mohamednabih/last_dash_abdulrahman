@extends('dashboard.master')
@section('title') {{awtTrans('الطلبات')}} @endsection
@section('style')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- Timelime example  -->
        <div class="row">
          <div class="col-md-12">
            <!-- The time line -->
            <div class="timeline">
              <!-- timeline item -->
              <div>
                <i class="fas fa-info bg-blue"></i>
                <div class="timeline-item">
                  <h3 class="timeline-header">{{awtTrans('تفاصيل الطلب')}}</h3>

                  <div class="timeline-body">
                    <div class="bill-info">
                      <ul>
                          <li class="text-bold">
                              <span>{{awtTrans('رقم الطلب')}}</span>
                              <span>:</span>
                              <span>{{$order->id}}</span>
                          </li>
                          <li class="text-bold">
                              <span>{{awtTrans('حالة الطلب')}}</span>
                              <span>:</span>
                              <span>{{order_status($order->status)}}</span>
                          </li>
                          {{-- <li>
                              <span>{{awtTrans('أسم المندوب')}}</span>
                              <span>:</span>
                              <span>محمود</span>
                          </li> --}}
                          <li>
                              <span>{{awtTrans('أسم العميل')}}</span>
                              <span>:</span>
                              <span>{{!is_null($order->user) ? $order->user->name : $order->user_name}}</span>
                          </li>
                          <li>
                              <span>{{awtTrans('جوال العميل')}}</span>
                              <span>:</span>
                              <span>{{!is_null($order->user) ? $order->user->phone : $order->user_phone}}</span>
                          </li>
                          @if(!is_null($order->date))
                            <li>
                                <span>{{awtTrans('تاريخ الطلب')}}</span>
                                <span>:</span>
                                <span>{{date('Y-m-d' , strtotime($order->date))}}</span>
                            </li>
                          @endif
                          {{-- <li>
                              <span>{{awtTrans('العنوان')}} </span>
                              <span>:</span>
                              <span>شارع عبد العزيز -الرياض</span>
                          </li> --}}
                          @if(!is_null($order->time))
                            <li>
                                <span>{{awtTrans('وقت الطلب')}} </span>
                                <span>:</span>
                                <span>{{$order->time}}</span>
                            </li>
                          @endif
                          <li class="text-bold">
                              <span>{{awtTrans('طريقة الدفع')}}</span>
                              <span>:</span>
                              <span>@if($order->payment_method == 'transfer') تحويل بنكي @elseif($order->payment_method == 'online') اون لاين @else كاش @endif</span>
                          </li>
                          <li class="text-bold">
                              <span>{{awtTrans('نوع الاشتراك')}}</span>
                              <span>:</span>
                              <span>@if($order->duration == 'urban') شهريا @elseif($order->duration == 'annual') سنويا @else مرة واحدة @endif</span>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END timeline item -->
              <!-- timeline item -->
              <div>
                <i class="fas fa-list bg-yellow"></i>
                <div class="timeline-item">
                  <h3 class="timeline-header">{{awtTrans('الطلبات')}}</h3>
                  <div class="timeline-body">
                        @foreach ($order->order_items as $item)
                            <div class="order-item">
                                <div class="img">
                                    <div class="img-c">
                                        <a data-fancybox data-caption="{{!is_null($order->section) ? $order->section->title_ar : $order->section_title_ar}}"
                                            href="{{is_null($order->section) ? url('/public/none.png') : url('' . $order->section->image)}}">
                                            <img src="{{is_null($order->section) ? url('/public/none.png') : url('' . $order->section->image)}}" alt="...">
                                        </a>
                                        <span class="count">{{$item->amount}}</span>
                                    </div>
                                    <span class="order-name">{{!is_null($order->section) ? $order->section->title_ar : $order->section_title_ar}}</span>
                                </div>
                                <div class="item-price">
                                    <span> <a href="https://www.google.com/maps/?q={{$item->lat}},{{$item->lng}}" target="_blanck">{{$item->address}}</a> </span>
                                </div>
                                <div class="item-total">
                                    <span> {{$item->name}}</span>
                                </div>
                                <div class="item-total">
                                    <span> {{$item->phone}}</span>
                                </div>
                                <div class="item-total">
                                    <span>@if(!empty($item->gender)) {{$item->gender == 'female' ? 'مصلى السيدات' : 'مصلى الرجال'}} @endif</span>
                                </div>
                                <div class="item-total">
                                    <span> <a href="" onclick="showNotes('{{$item->notes}}')" data-toggle="modal" data-target="#notes-model">ملاحظات</a></span>
                                </div>
                                <div class="item-price">
                                    <span> {{$order->section_price}}  </span> {{$item->amount}}X
                                </div>
                                <div class="item-total">
                                    <span> {{$order->section_price * $item->amount}} ريال سعودي</span>
                                </div>
                            </div>
                        @endforeach
                  </div>
                </div>
              </div>
              <!-- END timeline item -->
              <!-- timeline item -->
              <div>
                <i class="fas fa-wallet bg-purple"></i>
                <div class="timeline-item">
                  <h3 class="timeline-header">{{awtTrans('تفاصيل الفاتورة')}}</h3>
                  <div class="timeline-body">
                    <div class="bill-total">
                        <p>
                            <span>{{awtTrans('اجمالى الطلبات')}}</span>
                            <span>{{$order->section_price * $order->amount}} ر.س</span>
                        </p>
                        <p>
                            <span>{{awtTrans('رسوم التوصيل')}}</span>
                            <span>{{$order->delivery}} ر.س</span>
                        </p>
                        <p>
                            <span>{{awtTrans('القيمة المضافة')}}</span>
                            <span>{{$order->value_added}} ر.س</span>
                        </p>
                        <p>
                            <span>{{awtTrans('اجمالي الفاتورة')}}</span>
                            <span>{{$order->total_after_promo}} ر.س</span>
                        </p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END timeline item -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div>
      <!-- /.timeline -->

    </section>
    <!-- /.content -->
@endsection

@section('modal')
    <!-- confirm-del modal-->
    <div class="modal fade" id="notes-model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('ملاحظات')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h3 class="text-center" id="notes">

            </h3>
          </div>
        </div>
      </div>
    </div>
    <!--end confirm-del modal-->
@endsection

@section('script')
    <script>
        function showNotes(notes) {
            if(notes == '') notes = 'لا يوجد ملاحظات';
            $('#notes').html(notes);
        }
    </script>
@endsection
