<?php

namespace App\Http\Controllers\api;

#Basic
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\orderResource;
#resources
use App\Http\Resources\userResource;
#config->app
use Validator;
use Auth;
use Hash;
#Mail
use Mail;
use App\Mail\ActiveCode;
use App\Models\Order;
use App\Models\Order_item;
use App\Models\Section;
use App\User;

class orderController extends Controller
{
    #check promo code
    public function checkPromo(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id'      => 'required|exists:users,id',
            'code'         => 'required',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        # get discount
        $discount = check_promo_code($request->user_id, $request->code);
        #invalid promo code
        if ($discount == 0) return api_response(0, trans('api.invaildPromo'), $discount);
        #success promo code
        return api_response(1, trans('api.send'), $discount);
    }

    #store order
    public function storeOrder(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id'            => 'required|exists:users,id',
            'section_id'         => 'required|exists:sections,id',
            'amount'             => 'required',
            'delivery'           => 'required',
            'value_added'        => 'required',
            'total_before_promo' => 'required',
            'total_after_promo'  => 'required',
            'payment_method'     => 'required|in:cash,transfer,online',
            'duration'           => 'required|in:once,urban,annual',
            'date'               => 'nullable',
            'time'               => 'nullable',
            'code'               => 'nullable',
            'order_items'        => 'required',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #json decode
        $items = json_decode($request->order_items);
        #faild response
        if (empty($items)) return api_response(0, 'order_items is required and should be json');

        #store order
        $user    = User::whereId($request->user_id)->first();
        $section = Section::whereId($request->section_id)->first();
        $request->request->add([
            'status'           => 'new',
            'user_name'        => $user->name,
            'user_phone'       => $user->phone,
            'section_title_ar' => $section->title_ar,
            'section_title_en' => $section->title_en,
            'section_price'    => $section->price
        ]);
        $order = Order::create($request->except(['lang', 'code', 'order_items']));

        #store order items
        foreach ($items as $item) {
            Order_item::create([
                'order_id' => $order->id,
                'name'     => $item->name,
                'phone'    => $item->phone,
                'gender'   => $item->gender,
                'notes'    => $item->notes,
                'amount'   => $item->amount,
                'lat'      => $item->lat,
                'lng'      => $item->lng,
                'address'  => $item->address,
            ]);
        }

        #success response
        return api_response(1, trans('api.save'), new orderResource($order), ['notification_count' => user_notify_count($request->user_id)]);
    }

    #show all orders
    public function showAllOrders(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'status'  => 'nullable|in:current,refused,finish',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        if ($request->status == 'current')
            $orders = Order::where('user_id', $request->user_id)->whereNotIn('status', ['refused', 'cancel', 'finish'])->orderBy('id', 'desc')->get();

        elseif ($request->status == 'refused')
            $orders = Order::where('user_id', $request->user_id)->whereIn('status', ['refused', 'cancel'])->orderBy('id', 'desc')->get();

        elseif ($request->status == 'finish')
            $orders = Order::where('user_id', $request->user_id)->whereIn('status', ['finish'])->orderBy('id', 'desc')->get();

        else
            $orders = Order::where('user_id', $request->user_id)->orderBy('id', 'desc')->get();

        #success response
        return api_response(1, trans('api.send'), orderResource::collection($orders), ['notification_count' => user_notify_count($request->user_id)]);
    }

    #show order
    public function showOrder(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id'  => 'nullable|exists:users,id',
            'order_id' => 'required|exists:orders,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #success response
        return api_response(1, trans('api.save'), new orderResource(Order::whereId($request->order_id)->first()), ['notification_count' => user_notify_count($request->user_id)]);
    }
}
