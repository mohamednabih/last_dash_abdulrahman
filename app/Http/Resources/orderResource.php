<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\order_itemResource;
use App;

class orderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title         = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $section_title = App::getLocale() == 'en' ? 'section_title_en' : 'section_title_ar';
        $duration = ["once" => "مرة واحدة", "urban" => "شهريا", "annual" => "سنويا"];
        $payment  = ["cash" => "كاش", "transfer" => "تحويل بنكي", "online" => "اون لاين"];
        return [
            'id'                => (int)    $this->id,
            'status'            => (string) $this->status,
            'status_f'          => order_status($this->status),
            'user_name'         => !is_null($this->user) ? $this->user->name : (string)  $this->user_name,
            'user_phone'        => !is_null($this->user) ? $this->user->phone : (string)  $this->user_phone,
            'section_title'     => !is_null($this->section) ? $this->section->$title : (string)  $this->$section_title,
            'section_price'     => !is_null($this->section) ? $this->section->price : (string)  $this->section_price,
            'amount'            => (float)  $this->amount,
            'delivery'          => (float)  $this->delivery,
            'value_added'       => (float)  $this->value_added,
            'otal_before_promo' => (float)  $this->total_before_promo,
            'total_after_promo' => (float)  $this->total_after_promo,
            'payment_method'    => (string) $this->payment_method,
            'payment_method_f'  => isset($payment[$this->payment_method]) ? $payment[$this->payment_method] : 'كاش',
            'duration'          => (string) $this->duration,
            'duration_f'        => isset($duration[$this->duration]) ? $duration[$this->duration] : 'مرة واحدة',
            'date'              => is_null($this->date) ? '' : date('Y-m-d', strtotime($this->date)),
            'time'              => (string) $this->time,

            'order_services'    => order_itemResource::collection($this->order_items)
        ];
    }
}
