<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class order_itemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id'      => (int)    $this->id,
            'name'    => (string) $this->name,
            'phone'   => (string) $this->phone,
            'gender'  => (string) $this->gender,
            'notes'   => (string) $this->notes,
            'amount'  => (float)  $this->amount,
            'lat'     => (float)  $this->lat,
            'lng'     => (float)  $this->lng,
            'address' => (string)  $this->address,
        ];
    }
}
